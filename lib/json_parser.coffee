_ = require("lodash")
config = require("../config.coffee")

validJSONData = (data) ->
  try
    JSON.stringify data
  catch e
    return false
  true

validJSONString = (str) ->
  try
    JSON.parse str
  catch e
    return false
  true

chooseEpisodes = (episodes) ->
  _.filter episodes, goodShow

goodShow = (show) ->
  show.drm and show.episodeCount > 0

newList = (episodes) ->
  _.map episodes, showDetails

showDetails = (show) ->
  tmp = _.pick(show, [
    "image"
    "slug"
    "title"
  ])
  tmp.image = tmp.image.showImage
  tmp

module.exports =
  parseData: (input) ->
    json_test = validJSONData(input)
    if json_test
      all_episodes = input.payload
      return response: newList(chooseEpisodes(all_episodes))
    else
      return config.bad_json
    return

  parseString: (input) ->
    json_test = validJSONString(input)
    if json_test
      all_episodes = JSON.parse(input).payload
      return response: newList(chooseEpisodes(all_episodes))
    else
      return config.bad_json
    return
