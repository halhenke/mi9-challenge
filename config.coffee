module.exports =
  port: Number(process.env.PORT or 8080)
  bad_json:
    error: "Could not decode request: JSON parsing failed"
