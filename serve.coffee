http = require("http")
express = require("express")
bp = require('body-parser')
config = require("./config.coffee")
json_parser = require('./lib/json_parser.coffee')

app = express()
app.use bp.json()
http.createServer(app).listen config.port

app.route("/").post (req, res, next) ->
  res.contentType "application/json"
  res.send json_parser.parseData(req.body)
  return

# Catch malformed JSON queries
app.use (err, req, res, next) ->
  if err.message.match(/invalid json/)
    res.status 400
    res.contentType "application/json"
    res.send config.bad_json
  else
    next()
  return
