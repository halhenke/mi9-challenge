request = require("request")
config = require("../config")

sample_request = require("./sample_data/sample_request.json")
sample_response = require("./sample_data/sample_response.json")
json_parser = require("../lib/json_parser.coffee")
bad_json_input_string = "{'a': }"
bad_json_message = config.bad_json

describe "the JSON parsing module", ->

  describe "When parsing the sample request", ->
    it "should return the sample response", ->
      expect(json_parser.parseData(sample_request)).toEqual sample_response

  # slightly synthetic test
  describe "When parsing invalid JSON", ->
    it "should return the appropriate error message", ->
      expect(json_parser.parseString(bad_json_input_string)).toEqual bad_json_message

  describe "JSON Request/Response", ->
    request_options = undefined
    beforeEach ->
      request_options =
        url: "http://localhost:" + config.port
        json: sample_request

    describe "When posting the sample request data", ->
      beforeEach ->
        request_options.json = sample_request

      it "should return the sample response data", (done) ->
        request.post request_options, (error, response, body) ->
          expect(body).toEqual sample_response
          done()

    describe "When posting invalid JSON", ->
      beforeEach ->
        request_options.json = bad_json_input_string

      it "should return the appropriate error message", (done) ->
        request.post request_options, (error, response, body) ->
          expect(body).toEqual bad_json_message
          done()

      it "should return the appropriate error code", (done) ->
        request.post request_options, (error, response, body) ->
          expect(response.statusCode).toEqual 400
          done()
