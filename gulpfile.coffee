nodemon = require('gulp-nodemon')
gulp = require('gulp')

devOps =
  script: 'serve.coffee'
  ext: 'html js coffee'

gulp.task "serve", ->
  app.run()

gulp.task 'develop', ->
  nodemon(devOps)
    .on 'restart', ->
      console.log 'restarted!'

gulp.task 'default', ['develop']
